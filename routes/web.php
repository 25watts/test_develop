<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'TaskController@index')->name('task_index_path');

Route::group([
    'prefix' => 'task'
], function() {
    Route::get('create', 'TaskController@create')->name('task_create_path');
    Route::post('store', 'TaskController@store')->name('task_store_path');
    Route::get('show/{id}', 'TaskController@show')->name('task_show_path');
    Route::get('edit/{id}', 'TaskController@edit')->name('task_edit_path');
    Route::post('update', 'TaskController@update')->name('task_update_path');
    Route::get('update_status/{id}', 'TaskController@updateStatus')->name('task_update_status_path');
});

Route::group([
    'prefix' => 'category'
], function() {
    Route::get('create', 'CategoryController@create')->name('category_create_path');
    Route::post('store', 'CategoryController@store')->name('category_store_path');
});
