<nav class="navbar is-info @yield('color')" role="navigation" aria-label="main navigation">
  <div class="navbar-menu">
    <div class="navbar-start">
      <a class="navbar-item" href="{{ route('task_index_path') }}">Home</a>
    </div>
  </div>
</nav>
