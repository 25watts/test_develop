@extends('base')

@section('title')
  Crear nueva categoría
@endsection

@section('content')
  <form action="{{ route('category_store_path') }}" method="POST">
    {{ csrf_field() }}
    <div class="field">
      <label class="label" for="name">Nombre</label>
      <div class="control">
        <input name="name" class="input" type="text">
      </div>
    </div>
    <button class="button is-link" type="submit">Crear</button>
  </form>
@endsection
