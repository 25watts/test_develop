@extends('base')

@section('title')
  Crear nueva tarea
@endsection

@section('content')
  <form action="{{ route('task_update_path') }}" method="POST">
    {{ csrf_field() }}
    <div class="field">
      <label class="label" for="title">Nombre</label>
      <input name="title" class="input" type="text">
    </div>
    <div class="field">
      <label class="label" for="description">Descripción</label>
      <textarea name="description" class="textarea"></textarea>
    </div>
    <div class="field">
      <label class="label" for="category">Categoría</label>
      <div class="select">
        <select name="category_id">
          <option value="">Select a category</option>
          @foreach($categories as $category)
            <option value="{{ $category->id }}">{{ $category->name }}</option>
          @endforeach
        </select>
      </div>
    </div>
    <button class="button is-info" type="submit">Crear</button>
  </form>
@endsection