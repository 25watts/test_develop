@extends('base')

@section('title')
	{{ $task->title }}
@endsection

@section('color')
  @if($task->is_compleated)
    is-success
  @endif
@endsection

@section('content')
	<div class="content is-medium">
		<h1>{{ $task->title }}</h1>
	</div>
	<div class="content is-small">
		<h2>{{ $task->description }}</h2>
	</div>
	@if(!empty($task->category))
		<div class="content is-small">
			<h2>Categoría: {{ $task->category->name }}</h2>
		</div>
  @endif
  Creado en: {{ $task->created_at }}
  <br>
  Última vez modificado: {{ $task->updated_at }}
@endsection
