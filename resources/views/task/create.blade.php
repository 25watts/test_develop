@extends('base')

@section('title')
  Crear nueva tarea
@endsection

@section('content')
  <form action="{{ route('task_store_path') }}" method="POST">
    {{ csrf_field() }}
    <div class="field">
      <label class="label" for="title">Nombre</label>
      <div class="control">
        <input name="title" class="input {{ array_key_exists('title', $errors->messages()) ? 'is-danger' : '' }}" type="text">
      </div>
      @foreach($errors->getBag('default')->get('title') as $message)
        <p class="help is-danger">{{ $message }}</p>
      @endforeach
    </div>
    <div class="field">
      <label class="label" for="description">Descripción</label>
      <div class="control">
        <textarea name="description" class="textarea"></textarea>
      </div>
    </div>
    <div class="field">
      <label class="label" for="category">Categoría</label>
      <div class="control">
        <div class="select">
          <select name="category_id">
            <option value="">Select a category</option>
            @foreach($categories as $category)
              <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
          </select>
        </div>
      </div>
    </div>
    <button class="button is-link" type="submit">Crear</button>
  </form>
@endsection