@extends('base')

@section('title')
  Home
@endsection

@section('content')
  <a class="button" href="{{ route('task_create_path') }}">Crear tarea</a>
  <a class="button" href="{{ route('category_create_path') }}">Crear categoría</a>
  <table class="table is-fullwidth">
    <thead>
      <tr>
        <th>Completado</th>
        <th>Título</th>
        <th>Categoría</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach($tasks as $task)
        <tr>
          <td>
            <span class="icon">
              <a href="{{ route('task_update_status_path', $task->id) }}">
                <i class="{{ $task->is_compleated ? 'fas has-text-success' : 'far has-text-grey' }} fa-check-circle"></i>
              </a>
            </span>
          </td>
          <td>{{ $task->title }}</td>
          <td>
            @if(!empty($task->category))
              {{ $task->category->name }}
            @endif
          </td>
          <td>
            <a href="{{ route('task_show_path', $task->id) }}">Ver</a>
            <a href="{{ route('task_edit_path') }}">Editar</a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
@endsection
